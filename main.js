const UNCOMPLETED_LIST_BOOK ="incompleteBookshelfList";
const COMPLETED_LIST_BOOK="completeBookshelfList";
const BOOK_ID = "bookId";




function makeListBook( judul /* string */, author /* string */, ytahun /* string */,isCompleted /* boolean */){

    const textTitle = document.createElement("h2");
    textTitle.innerText = judul;

    const textAuthor = document.createElement("div");
    textAuthor.innerText = author;

    const textyTahun = document.createElement("div");
    textyTahun.innerText = ytahun;

    const textContainer = document.createElement("div");
    textContainer.classList.add("inner")
    textContainer.append(textTitle,textAuthor, textyTahun);

    const container = document.createElement("div");
    container.classList.add("item", "shadow")
    container.append(textContainer);

    if(isCompleted){
        container.append( // selesai dibaca
             createUndoButton(),
            createTrashButton()
        )
    }else{
        container.append(// belum selesai dibaca
            createTrashButton(),
            createCheckButton()
        )
    }
    return container;
}

function addBook(){
    const uncompletedBooklist = document.getElementById(UNCOMPLETED_LIST_BOOK);
    const completedBooklist = document.getElementById(COMPLETED_LIST_BOOK);
   
  
    const txtJudul = document.getElementById("inputBookTitle").value;
    const txtAuthor = document.getElementById("inputBookAuthor").value;
    const txtTahun  = document.getElementById("inputBookYear").value;
    const txtisCompleted = document.getElementById("inputBookIsComplete").checked;
    
    const book = makeListBook(txtJudul, txtAuthor,txtTahun, txtisCompleted);
    const bookObject = composeTodoObject(txtJudul, txtAuthor,txtTahun, txtisCompleted)
    
    book[BOOK_ID] = bookObject.id;
    books.push(bookObject);

    if (txtisCompleted == false){
    
        uncompletedBooklist.append(book);
        

    }else{
    
        completedBooklist.append(book);
  
    }
    updateDataToStorage();



}

function addBookToCompleted(taskElement /* HTMLELement */) {
    const listCompleted = document.getElementById(COMPLETED_LIST_BOOK);

    const txtJudul = taskElement.querySelector(".inner > h2").innerText;
    const txtAuthor = taskElement.querySelector(".inner > div").innerText;
    const txtTahun = taskElement.querySelector(".inner > div").innerText;


    const completeBook = makeListBook(txtJudul, txtAuthor, txtTahun, true);


    const book = findBook(taskElement[BOOK_ID]);
    book.isCompleted = true;
    completeBook[BOOK_ID] = book.id;
    
    listCompleted.append(completeBook);
    taskElement.remove();

    updateDataToStorage();
}


function undoBookFromCompleted(taskElement /* HTMLELement */){
    const unlistCompleted = document.getElementById(UNCOMPLETED_LIST_BOOK);
    
    const txtJudul = taskElement.querySelector(".inner > h2").innerText;
    const txtAuthor = taskElement.querySelector(".inner > div").innerText;
    const txtTahun = taskElement.querySelector(".inner > div").innerText;
    const uncompleteBook = makeListBook(txtJudul, txtAuthor,txtTahun,false);

    const book = findBook(taskElement[BOOK_ID])
    book.isCompleted = false;
    uncompleteBook[BOOK_ID]= book.id;

    unlistCompleted.append(uncompleteBook);
    taskElement.remove();

    updateDataToStorage();
}

function removeBookFromList(taskElement /* HTMLELement */){
    const bookPosition = findBookIndex(taskElement[BOOK_ID]);
    books.splice(bookPosition, 1);

    taskElement.remove();
    updateDataToStorage();
}

function refreshData() {
    const listUncompleted = document.getElementById(UNCOMPLETED_LIST_BOOK);
    let listCompleted = document.getElementById(COMPLETED_LIST_BOOK);

    for(book of books){
        const newBook = makeListBook(book.judul, book.author, book.ytahun, book.isCompleted);
        newBook[BOOK_ID] = book.id;

        if(book.isCompleted){
            listCompleted.append(newBook);
            taskElement.remove();
        } else {
            listUncompleted.append(newBook);
            taskElement.remove();
        }
    }
}


function searchBook(){
    const listUncompleted = document.getElementById(UNCOMPLETED_LIST_BOOK);
    let listCompleted = document.getElementById(COMPLETED_LIST_BOOK);

    const txtJudul = document.getElementById("searchBookTitle").value;
    console.log(txtJudul);
    for(book of books){
        if(book.judul == txtJudul){
            const newBook = makeListBook(book.judul, book.author, book.ytahun, book.isCompleted);
            newBook[BOOK_ID] = book.id;

            if(book.isCompleted){
                listCompleted.append(newBook);
            } else {
                listUncompleted.append(newBook);
            }
            
        }
    }


}


function createUndoButton() {
    return createButton("undo-button", function(event){
        undoBookFromCompleted(event.target.parentElement);
    });
}

function createTrashButton(){
    return createButton("trash-button", function(event){
        removeBookFromList(event.target.parentElement);
    });
}

function createCheckButton(){
    return createButton("check-button", function(event){
        addBookToCompleted(event.target.parentElement);
    })
}
function createButton(buttonTypeClass /* string */, eventListener /* callback function */) {
    const button = document.createElement("button");
    button.classList.add(buttonTypeClass);
    button.addEventListener("click", function (event) {
        eventListener(event);
    });
    return button;
}

